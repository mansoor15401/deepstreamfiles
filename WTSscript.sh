git clone https://github.com/ultralytics/ultralytics.git
git clone https://github.com/marcoslucianops/DeepStream-Yolo
cd /content/DeepStream-Yolo
git checkout 68f762d5bdeae7ac3458529bfe6fed72714336ca
cp utils/gen_wts_yoloV8.py /content/ultralytics
cd /content/ultralytics
pip3 install -r requirements.txt

cp '/content/drive/MyDrive/More-Drowziness-Dataset+runs/runs/detect/train/weights/best.pt' .
python3 gen_wts_yoloV8.py -w best.pt

